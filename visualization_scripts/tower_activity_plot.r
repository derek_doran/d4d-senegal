require(RColorBrewer)
require(cluster)
require(ggplot2)

#requires data frames from merge_tower_data_with_position.
cols <- colorRampPalette(brewer.pal(3,"RdYlBu"))(100)#(nrow(degrees))
 #towers_grouped$pagerank <- V(g)$pagerank
 plot(
  ggplot(data=td,aes(x=out_lon,y=out_lat)) + 
    geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
    geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
    geom_segment(aes(x=out_lon,y=out_lat,xend=in_lon,yend=in_lat),col="orange",alpha=0.6, 
                 size=8*td$total_duration/max(td$total_duration))+
    geom_point(data=antenna,aes(x=lon,y=lat),size=6*V(g)$pagerank,color="green",alpha=0.7)+
    scale_color_gradient(low="green", high="lightblue")+
    theme_bw() +
    ggtitle("Calls between towers") + 
    theme(
      plot.title = element_text(lineheight=.8, face="bold"),
      axis.line=element_blank(),
      axis.text.x=element_blank(),
      axis.text.y=element_blank(),
      axis.ticks=element_blank(),
      axis.title.x=element_blank(),
      axis.title.y=element_blank(),
      legend.position="none",
      panel.background=element_blank(),
      panel.border=element_blank(),
      panel.grid.major=element_blank(),
      panel.grid.minor=element_blank(),
      plot.background=element_blank()
    )
)