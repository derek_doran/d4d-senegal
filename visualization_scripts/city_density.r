require(ggplot2)
require(ggmap)
require(maptools)
source('multiplot.r')

senegal <- readShapeSpatial("SEN_adm/SEN_adm0.shp")
senegal <- fortify(senegal)
roads <- fortify(readShapeSpatial("Senegal_Roads/Senegal_Roads.shp"))

cities_q0 <- ggplot(data=cities,aes(x=lon,y=lat)) + 
  geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
  geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
  geom_point(col="white",size=1) + 
  theme_bw() +
  scale_fill_gradient(low = "blue",high = "yellow") + 
  stat_density2d(aes(fill=..level..),geom="polygon",h=0.46,alpha=0.5) +
  ggtitle("All Cities") + 
  theme(
    plot.title = element_text(lineheight=.8, face="bold"),
    axis.line=element_blank(),
    axis.text.x=element_blank(),
    axis.text.y=element_blank(),
    axis.ticks=element_blank(),
    axis.title.x=element_blank(),
    axis.title.y=element_blank(),
    legend.position="none",
    panel.background=element_blank(),
    panel.border=element_blank(),
    panel.grid.major=element_blank(),
    panel.grid.minor=element_blank(),
    plot.background=element_blank())

cities_q25 <- ggplot(data=cities[which(cities$pop > 2207), ],aes(x=lon,y=lat)) + 
    geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
    geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
    geom_point(col="white",size=1) + 
    theme_bw() +
    scale_fill_gradient(low = "blue",high = "yellow") + 
    stat_density2d(aes(fill=..level..),geom="polygon",h=0.46,alpha=0.5) +
    ggtitle("25% Quantile") + 
    theme(
      plot.title = element_text(lineheight=.8, face="bold"),
      axis.line=element_blank(),
      axis.text.x=element_blank(),
      axis.text.y=element_blank(),
      axis.ticks=element_blank(),
      axis.title.x=element_blank(),
      axis.title.y=element_blank(),
      legend.position="none",
      panel.background=element_blank(),
      panel.border=element_blank(),
      panel.grid.major=element_blank(),
      panel.grid.minor=element_blank(),
      plot.background=element_blank())

cities_q50 <- ggplot(data=cities[which(cities$pop > 4980), ],aes(x=lon,y=lat)) + 
  geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
  geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
  geom_point(col="white",size=1) + 
  theme_bw() +
  scale_fill_gradient(low = "blue",high = "yellow") + 
  stat_density2d(aes(fill=..level..),geom="polygon",h=0.46,alpha=0.5) +
  ggtitle("50% Quantile") + 
  theme(
    plot.title = element_text(lineheight=.8, face="bold"),
    axis.line=element_blank(),
    axis.text.x=element_blank(),
    axis.text.y=element_blank(),
    axis.ticks=element_blank(),
    axis.title.x=element_blank(),
    axis.title.y=element_blank(),
    legend.position="none",
    panel.background=element_blank(),
    panel.border=element_blank(),
    panel.grid.major=element_blank(),
    panel.grid.minor=element_blank(),
    plot.background=element_blank())

cities_q75 <- ggplot(data=cities[which(cities$pop > 11003), ],aes(x=lon,y=lat)) + 
  geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
  geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
  geom_point(col="white",size=1) + 
  theme_bw() +
  scale_fill_gradient(low = "blue",high = "yellow") + 
  stat_density2d(aes(fill=..level..),geom="polygon",h=0.46,alpha=0.5) +
  ggtitle("75% Quantile") + 
  theme(
    plot.title = element_text(lineheight=.8, face="bold"),
    axis.line=element_blank(),
    axis.text.x=element_blank(),
    axis.text.y=element_blank(),
    axis.ticks=element_blank(),
    axis.title.x=element_blank(),
    axis.title.y=element_blank(),
    legend.position="none",
    panel.background=element_blank(),
    panel.border=element_blank(),
    panel.grid.major=element_blank(),
    panel.grid.minor=element_blank(),
    plot.background=element_blank())

multiplot(cities_q0, cities_q25, cities_q50, cities_q75, 
          layout=matrix(c(1,2,3,4), nrow=2, byrow=TRUE))
rm(cities_q0)
rm(cities_q25)
rm(cities_q50)
rm(cities_q75)
quantiles <- ggplot(data=cities, aes(x=lon,y=lat)) + 
  geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
  geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
  geom_point(color="red",size=1) +
  geom_point(data=cities[which(cities$pop > 11003), ],col="red",size=1) + 
  geom_point(data=cities[which(cities$pop > 4980 & cities$pop < 11003), ],col="yellow",size=1) + 
  geom_point(data=cities[which(cities$pop > 2205 & cities $pop < 4180), ],col="green",size=1) + 
  theme(  
    plot.title = element_text(lineheight=.8, face="bold"),
    axis.line=element_blank(),
    axis.text.x=element_blank(),
    axis.text.y=element_blank(),
    axis.ticks=element_blank(),
    axis.title.x=element_blank(),
    axis.title.y=element_blank(),
    legend.position="none",
    panel.background=element_blank(),
    panel.border=element_blank(),
    panel.grid.major=element_blank(),
    panel.grid.minor=element_blank(),
    plot.background=element_blank()
  )

  