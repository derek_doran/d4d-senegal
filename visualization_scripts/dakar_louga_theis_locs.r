require(RColorBrewer)
require(cluster)
require(ggplot2)
x <- data.frame(
  long=c(-17.36,-16.22,-16.967,-17.455,-17.391,-16.49,-16.253,-16.272,-16.233,-13.667), 
  lat=c(14.75,15.62,14.783,14.713,14.765,16.018,14.182,12.583,14.633,13.771),
  group=rep(1,10))

# 2	Grand Dakar  wikipedia article, Dakar	2,352,057	14.713 / -17.455
# 3	Pikine  wikipedia article, Dakar	874,062	14.765 / -17.391
# 6	Saint-Louis  wikipedia article, Saint-Louis	176,000	16.018 / -16.49
# 7	Kaolack  wikipedia article, Kaolack	172,305	14.182 / -16.253
# 8	Ziguinchor  wikipedia article, Ziguinchor	159,778	12.583 / -16.272
# 9	Tiébo, Diourbel	100,289	14.633 / -16.233
# 10	Tambacounda  wikipedia article, Tambacounda	78,800	13.771 / -13.667

plot(
  ggplot() + 
    geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
    geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
    geom_point(data=x,aes(x=long,y=lat),size=4,color="red")+
    theme_bw() +
    ggtitle("10 Most Populated Cities") + 
    theme(
      plot.title = element_text(lineheight=.8, face="bold"),
      axis.line=element_blank(),
      axis.text.x=element_blank(),
      axis.text.y=element_blank(),
      axis.ticks=element_blank(),
      axis.title.x=element_blank(),
      axis.title.y=element_blank(),
      legend.position="none",
      panel.background=element_blank(),
      panel.border=element_blank(),
      panel.grid.major=element_blank(),
      panel.grid.minor=element_blank(),
      plot.background=element_blank()
    )
)