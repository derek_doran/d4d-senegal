require(sqldf)
require(scatterplot3d)
require(geosphere)

#ISOLATION
isolation_1 <- sqldf('SELECT A.*, B.outDegree AS totalCalls
                      FROM gephi A INNER JOIN towers_grouped B
                      ON A.Source = B.arr_id')
isolation_1$callProp <- isolation_1$calls / isolation_1$totalCalls
isolation_2 <- isolation_1[isolation_1$Source == isolation_1$Target,]
isolation <- sqldf('SELECT A.Source AS arr_id, A.calls AS selfCalls, A.totalCalls, A.callProp AS selfProp, B.lon, B.lat
                    FROM isolation_2 A INNER JOIN towers_grouped B
                    ON A.Source = B.arr_id')
isolation$logCalls <- log(isolation$totalCalls)
remove(isolation_1, isolation_2)

#7.5x6 inches
colfunc <- colorRampPalette(c("red", "blue"))
cols <- colfunc(length(unique(isolation$selfProp)))
vals <- round(sort(unique(isolation$selfProp),dec=T),3)
for(i in 1:123){
  isolation$col[i] <- cols[which(vals == round(isolation$selfProp[i],3))]
}

scatterplot3d(isolation$lon,isolation$lat,isolation$selfProp,type="h",
              color=isolation$col,highlight.3d=F,pch=16,angle = 80,cex.axis=1,
              axis = T,box = F,scale.y=0.5,xlab="Longitude",lab.z=c(2,4,4),ylab="Latitude",
              zlab="Percenage of Calls (Internal)")

par(mar=c(4.1, 4.1, 2.1, 2.1))
plot(isolation$lon,isolation$lat,col=isolation$col,cex=1.7,pch=16,xlab="Longitude",ylab="Latitude")

scatterplot(isolation$selfProp,isolation$lon,pch=16,lwd=5,
            smoother.args=list(lty=3, lwd=4, lty.spread=3, lwd.spread=1, 
                               span=0.4, degree=2, family="gaussian", iterations=30),
            spread=F,xlab="Percentage of Calls (Internal)",cex=1.5,ylab="Longitude",cex.lab=1.6)

plot(isolation$selfProp,distance$logCalls,col=isolation$col,pch=16,cex=1.4)

plot(log(isolation$totalCalls),isolation$selfProp,pch=16,col=isolation$col,
     xlab="Total Calls Placed (Log-scale)", 
     ylab="Proportion of Internal Calls")
abline(lm(isolation$selfProp~log(isolation$totalCalls)),col="red",lwd=4)

isolate_order <- order(isolation$selfProp,decreasing = T)
mean_isolation_around_isolate <- rep(0,123)
mean_dist_around_isolate <- rep(0,123)
k=1
for(j in isolate_order){
  closest5 <- rep(-1,122)
  closest5dist <- rep(10E9,122)
  for(i in 1:123){
    if(i != j){
      d <- distHaversine(cbind(distance$lon[j],distance$lat[j]), 
                         cbind(distance$lon[i],distance$lat[i]))/1000  
      w <- which(closest5dist > d)
      if(length(w) > 0){
        idx <- which(closest5dist == max(closest5dist[w]))
        closest5[idx[1]] <- i
        closest5dist[idx[1]] <- d
      } 
    }
  }
  mean_dist_around_isolate[k] <- mean(closest5dist)
  mean_isolation_around_isolate[k] <- median(isolation$selfProp[closest5])
  k <- k+1
}