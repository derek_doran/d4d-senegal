require(RColorBrewer)
require(cluster)
require(ggplot2)
require(sqldf)
require(scatterplot3d)

par(mar=c(2.1, 4.1, 2.1, 2.1))
#x <- sort(rlnorm(n = 123,meanlog = mean(log(distance$num_calls)),sdlog = sd(log(distance$num_calls))),decreasing=T)
plot(sort(distance$num_calls,decreasing = T),pch=12,col="orange",cex=1,cex.lab=1.2,cex.axis=1.2,
     log="xy",xlab="",ylab="Number of Calls Placed")
x <- lognorm_callvolume
lines(x,lwd=5,col="black")

plot(x,col="black",lwd=2,typ="l",xlab="",ylab="Number of Calls Placed", cex.lab=1.2,cex.axis=1.2)

xmin <- min(which(x > mean(x) & x < mean(x)+2*sd(x)))
xmax <- max(which(x > mean(x) & x < mean(x)+2*sd(x)))

cord.x <- 1:xmin
cord.y <- x[cord.x]
cord.x <- c(1,cord.x,xmin)
cord.y <- c(0,cord.y,0)
polygon(cord.x,cord.y,col="red")
cord.x <- xmin:xmax
cord.y <- x[cord.x]
cord.x <- c(xmin,cord.x,xmax)
cord.y <- c(0,cord.y,0)
polygon(cord.x,cord.y,col="blue")
cord.x <- xmax:length(x)
cord.y <- x[cord.x]
cord.x <- c(xmax,cord.x,length(x))
cord.y <- c(0,cord.y,0)
polygon(cord.x,cord.y,col="purple",angle = 60)
remove(xmin,xmax,cord.x,cord.y,x)

text(x=68, y = 1E8, label ="Dakar and Neighboring Arrondissements", col="red",cex=1)
text(x=57, y = 8E7, label ="High Volume Outside of Dakar", col="blue",cex=1)
text(x=59, y = 6E7, label ="Long Tail of Small Call Volumes", col="purple",cex=1)

#7x5 inches
#points(sort(distance$num_calls,decreasing = T),pch=16,col="black",cex=0.4)

#7.5x6 inches
scatterplot3d(distance$lon,distance$lat,distance$num_calls,type="h",
              color=distance$col,highlight.3d=F,pch=19,angle = 60,cex.axis=1,
              axis = T,box = F,scale.y=0.5,xlab="Longitude",lab.z=c(2,4,4),ylab="Latitude",zlab="Number of Calls")
