closeAllConnections()

library(maps) 
#gpclibPermit() #needs to be true - load maptools before gpclib
library(ggplot2)
library(plyr)
library(RColorBrewer)
library(scales) 
library(sqldf)
library(ggmap)
library(geosphere)
library(corrgram)
library(mclust)


###cell tower data###

cell <- td
cell$out_arr_id <- NULL
cell$in_arr_id <- NULL
towers <- read.csv("csv_files/tower_locations.csv", header=T)
cell_within <- cell[cell$out_site_id == cell$in_site_id,]
cell_btwn <- cell[cell$out_site_id != cell$in_site_id,]

towers <- sqldf('SELECT A.*, B.num_calls AS callsWithin
                FROM towers A INNER JOIN cell_within B
                ON A.site_id = B.out_site_id')
senmap <- get_map(location = 'Senegal', zoom = 7)
tower_map <- ggplot() + geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
  geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") +
  geom_point(aes(x=lon, y=lat, size=sqrt(callsWithin), alpha=0.8), 
             fill="darkorange", colour="black", pch=21, data=towers)
tower_map

towers_g1 <- sqldf('SELECT out_site_id AS site_id, SUM(num_calls) AS outDegree
                   FROM cell GROUP BY out_site_id')
towers_g2 <- sqldf('SELECT A.site_id, B.arr_id, B.lon, B.lat, A.outDegree
                   FROM towers_g1 A INNER JOIN towers B
                   ON A.site_id = B.site_id')
towers_grouped <- sqldf('SELECT arr_id, AVG(lon) AS lon, AVG(lat) AS lat, SUM(outDegree) AS outDegree
                        FROM towers_g2 GROUP BY arr_id')
remove(towers_g1, towers_g2)

group_map <- ggmap(senmap) + 
  geom_point(aes(x=lon, y=lat, size=log(outDegree), alpha=0.8), 
             fill="darkblue", colour="black", pch=21, data=towers_grouped) +
  scale_size_continuous(range = c(3,7))
group_map

group_map2 <- ggmap(senmap) +
  geom_point(aes(x=lon, y=lat, fill=sqrt(outDegree), alpha=0.8),
             size=5, colour="black", pch=21, data=towers_grouped) +
  scale_fill_gradientn(colours=c("darksalmon","darkred"))
group_map2

#Prep data for loading into Gephi#
gephi_places <- sqldf('SELECT arr_id, AVG(lat) AS lat, AVG(lon) AS lng
                      FROM towers GROUP BY arr_id')
write.csv(gephi_places, "nodes.csv", row.names=FALSE)
gephi_1 <- sqldf('SELECT A.*, B.arr_id AS out_arr_id
                 FROM cell A INNER JOIN towers B ON
                 A.out_site_id = B.site_id')
gephi_2 <- sqldf('SELECT A.*, B.arr_id AS in_arr_id
                 FROM gephi_1 A INNER JOIN towers B ON
                 A.in_site_id = B.site_id')
gephi <- sqldf('SELECT out_arr_id AS Source, in_arr_id AS Target, SUM(num_calls) AS calls
               FROM gephi_2 GROUP BY out_arr_id, in_arr_id')
write.csv(gephi, "edges.csv", row.names=FALSE)
remove(gephi_places, gephi_1, gephi_2)

#-----clustering setup: area level-----#

#ISOLATION
isolation_1 <- sqldf('SELECT A.*, B.outDegree AS totalCalls
                     FROM gephi A INNER JOIN towers_grouped B
                     ON A.Source = B.arr_id')
isolation_1$callProp <- isolation_1$calls / isolation_1$totalCalls
isolation_2 <- isolation_1[isolation_1$Source == isolation_1$Target,]
isolation <- sqldf('SELECT A.Source AS arr_id, A.calls AS selfCalls, A.totalCalls, A.callProp AS selfProp, B.lon, B.lat
                   FROM isolation_2 A INNER JOIN towers_grouped B
                   ON A.Source = B.arr_id')
isolation$logCalls <- log(isolation$totalCalls)
remove(isolation_1, isolation_2)

#ISOLATION MAP
isolation_map <- ggplot()+
  geom_polygon(data=senegal,aes(x=long,y=lat,group=group)) + 
  geom_path(data=roads,aes(x=long,y=lat,group=group),col="white") + 
  geom_point(data=isolation,aes(x=lon, y=lat, fill=selfProp),size=5, colour="black", pch=21) +
  scale_fill_gradientn(colours=c("white","darkred")) +
  ggtitle("Self-Sufficiency: % of Calls Within Area")
isolation_map

#PARETO PARTNERS
pareto_1 <- sqldf('SELECT A.*, B.outDegree AS totalCalls
                  FROM gephi A INNER JOIN towers_grouped B
                  ON A.Source = B.arr_id')
pareto_1 <- pareto_1[order(pareto_1$Source, -pareto_1$calls),]

for(i in 1:nrow(pareto_1)){
  if(i==1){
    pareto_1[i,5] = pareto_1[i,3] / pareto_1[i,4]
  }
  else{
    if(pareto_1[i-1,1]==pareto_1[i,1]){
      pareto_1[i,5] = pareto_1[i-1,5] + (pareto_1[i,3] / pareto_1[i,4])
    }
    else{
      pareto_1[i,5] = pareto_1[i,3] / pareto_1[i,4]
    }
  }
}
colnames(pareto_1)[5] = "cumeProbInclSelf"

isolation$exclSelf <- isolation$totalCalls - isolation$selfCalls
pareto_2 <- sqldf('SELECT A.*, B.exclSelf AS totalCalls
                  FROM gephi A INNER JOIN isolation B
                  ON A.Source = B.arr_id
                  WHERE A.Source <> A.Target')
pareto_2 <- pareto_2[order(pareto_2$Source, -pareto_2$calls),]

for(i in 1:nrow(pareto_2)){
  if(i==1){
    pareto_2[i,5] = pareto_2[i,3] / pareto_2[i,4]
  }
  else{
    if(pareto_2[i-1,1]==pareto_2[i,1]){
      pareto_2[i,5] = pareto_2[i-1,5] + (pareto_2[i,3] / pareto_2[i,4])
    }
    else{
      pareto_2[i,5] = pareto_2[i,3] / pareto_2[i,4]
    }
  }
}
colnames(pareto_2)[5] = "cumeProbExclSelf"

pareto_1b <- sqldf('SELECT Source AS arr_id, COUNT(*) AS paretoIncl
                   FROM pareto_1 WHERE cumeProbInclSelf <= 0.8
                   GROUP BY arr_id')
pareto_2b <- sqldf('SELECT Source AS arr_id, COUNT(*) AS paretoExcl
                   FROM pareto_2 WHERE cumeProbExclSelf <= 0.8
                   GROUP BY arr_id')
pareto <- sqldf('SELECT A.arr_id, A.lat, A.lon, A.logCalls, A.selfProp,
                B.paretoIncl, C.paretoExcl
                FROM isolation A INNER JOIN pareto_1b B
                ON A.arr_id = B.arr_id INNER JOIN pareto_2b C
                ON B.arr_id = C.arr_id')
remove(pareto_1, pareto_1b, pareto_2, pareto_2b)

pareto_map <- ggmap(senmap) +
  geom_point(aes(x=lon, y=lat, fill=paretoExcl),
             size=5, colour="black", pch=21, data=pareto) +
  scale_fill_gradientn(colours=c("white","darkblue")) +
  ggtitle("Partnership: # of Partner Areas in Top 80% of Calls")
pareto_map

#DISTANCE
distance_1 <- sqldf('SELECT B.*, A.lat AS latO, A.lon AS lonO, C.lat AS latD, C.lon AS lonD
                    FROM towers_grouped A INNER JOIN gephi B
                    ON A.arr_id = B.Source INNER JOIN towers_grouped C
                    ON B.Target = C.arr_id')
#distance is in kilometers
distance_1$dist <- distHaversine(cbind(distance_1$lonO, distance_1$latO), cbind(distance_1$lonD, distance_1$latD)) / 1000
distance_1$dwDist <- distance_1$dist * distance_1$calls
distance_2 <- sqldf('SELECT A.*, B.outDegree as totalCalls
                    FROM distance_1 A INNER JOIN towers_grouped B
                    ON A.Source = B.arr_id')
distance_2$callProp <- distance_2$calls / distance_2$totalCalls
distance_2$dwAvgDist <- distance_2$callProp * distance_2$dist
distance_3 <- sqldf('SELECT Source AS arr_id, AVG(dwDist) AS totalDWD, SUM(dwAvgDist) AS avgDWD
                    FROM distance_2 GROUP BY arr_id')
distance <- sqldf('SELECT A.*, B.avgDWD, B.totalDWD
                  FROM pareto A INNER JOIN distance_3 B
                  ON A.arr_id = B.arr_id')
distance$logTotalDWD <- log(distance$totalDWD)
remove(distance_1, distance_2, distance_3)

#DISTANCE MAP
distance_map <- ggmap(senmap) +
  geom_point(aes(x=lon, y=lat, fill=totalDWD),
             size=5, colour="black", pch=21, data=distance) +
  scale_fill_gradientn(colours=c("white","darkgreen")) +
  ggtitle("DWD: Total Call Volume x Distance")
distance_map

distance_map2 <- ggmap(senmap) +
  geom_point(aes(x=lon, y=lat, fill=avgDWD),
             size=5, colour="black", pch=21, data=distance) +
  scale_fill_gradientn(colours=c("white","darkorange4")) +
  ggtitle("Distance: Weighted Avg. Distance of Calls")
distance_map2

#CLUSTERING
clustering <- distance
corrgram(clustering[,4:10], order=TRUE, lower.panel=panel.shade, upper.panel=panel.pie, text.panel=panel.txt)
#select paretoExcl, avgDWD, logTotalDWD, and selfProp as variables
#some concern about correlation between selfProp and logTotalDWD

#standardization
col_list <- c("paretoExcl", "avgDWD", "logTotalDWD", "selfProp", "logCalls")

start_range <- ncol(clustering)+1
end_range <- ncol(clustering)+length(col_list)

clustering[,start_range:end_range]<-sapply(clustering[,col_list], scale, scale=TRUE)

new_col_list<-paste("STD_", sep="", col_list)

colnames(clustering)[start_range:end_range] <- new_col_list

#FMM
plot.FMM = function(fit,boxplot=F)
{
  require(lattice)
  #number of clusters
  k = ncol(fit$parameters$mean)
  #number of variables
  p = nrow(fit$parameters$mean)
  plotdat = data.frame(
    mu=as.vector(t(fit$parameters$mean)),
    clus=factor(rep(1:k, p)),
    var=factor( 0:(p*k-1) %/% k, labels=row.names(fit$parameters$mean))
  )
  print(dotplot(var~mu|clus, data=plotdat,
                panel=function(...){
                  panel.dotplot(...)
                  panel.abline(v=0, lwd=.1)
                },
                layout=c(k,1),
                xlab="Cluster Mean"
  ))
  invisible(plotdat)
}

#vars <- c("STD_paretoExcl", "STD_avgDWD", "STD_selfProp", "STD_logCalls")
vars <- c("STD_paretoExcl", "STD_avgDWD", "STD_logTotalDWD", "STD_selfProp")
x <- clustering[, vars]
FMM1 <- Mclust(x)
#modelNames=c("EII", "VII", "EEI", "VEI", "EVI", "VVI", "EEE", "EEV", "VEV", "VVV"))
plot(FMM1)
#4 vars: 4 cluster, EEV
#4 vars, logCalls: 3 cluster, VEV

FMM1 <- Mclust(x, G=4)
summary(FMM1)
FMM1$parameters$mean
plot.FMM(FMM1)

clustering$cluster <- FMM1$classification

#K-means
summary.kmeans = function(fit)
{
  p = ncol(fit$centers)
  k = nrow(fit$centers)
  n = sum(fit$size)
  sse = sum(fit$withinss)
  xbar = t(fit$centers)%*%fit$size/n
  ssb = sum(fit$size*(fit$centers - rep(1,k) %*% t(xbar))^2)
  print(data.frame(
    n=c(fit$size, n),
    Pct=(round(c(fit$size, n)/n,2)),
    round(rbind(fit$centers, t(xbar)), 2),
    RMSE = round(sqrt(c(fit$withinss/(p*fit$size-1), sse/(p*(n-k)))), 4)
  ))
  cat("SSE = ", sse, "; SSB = ", ssb, "\n")
  cat("R-Squared = ", ssb/(ssb+sse), "\n")
  cat("Pseudo F = ", (ssb/(k-1))/(sse/(n-k)), "\n\n");
  invisible(list(sse=sse, ssb=ssb, Rsqr=ssb/(ssb+sse), F=(ssb/(k-1))/(sse/(n-k))))
}
plot.kmeans = function(fit,boxplot=F)
{
  require(lattice)
  p = ncol(fit$centers)
  k = nrow(fit$centers)
  plotdat = data.frame(
    mu=as.vector(fit$centers),
    clus=factor(rep(1:k, p)),
    var=factor( 0:(p*k-1) %/% k, labels=colnames(fit$centers))
  )
  print(dotplot(var~mu|clus, data=plotdat,
                panel=function(...){
                  panel.dotplot(...)
                  panel.abline(v=0, lwd=.1)
                },
                layout=c(k,1),
                xlab="Cluster Mean"
  ))
  invisible(plotdat)
}
fit.3 <- kmeans(x, 3, nstart=100)
fit.4 <- kmeans(x, 4, nstart=100)
fit.5 <- kmeans(x, 5, nstart=100)
summary(fit.3)
summary(fit.4)
summary(fit.5)
plot(fit.4)

clustering$cluster <- fit.4$cluster

#CLUSTER MAP
cluster_map <- ggmap(senmap) +
  geom_point(aes(x=lon, y=lat, fill=as.factor(cluster)),
             size=5, colour="black", pch=21, data=clustering)
cluster_map

par(mfrow=c(2,2))
isolation_map
pareto_map
distance_map
distance_map2

#-----clustering setup: tower level-----#

#ISOLATION
isol_tower_1 <- sqldf('SELECT out_site_id AS site_id, SUM(num_calls) AS totalCalls
                      FROM cell GROUP BY out_site_id')
isol_tower_2 <- sqldf('SELECT A.*, B.totalCalls
                      FROM cell A INNER JOIN isol_tower_1 B
                      ON A.out_site_id = B.site_id')
isol_tower_2$callProp <- isol_tower_2$num_calls / isol_tower_2$totalCalls
isol_tower_3 <- isol_tower_2[isol_tower_2$out_site_id == isol_tower_2$in_site_id,]
isol_tower <- sqldf('SELECT A.out_site_id AS site_id, A.num_calls AS selfCalls, A.totalCalls, A.callProp, B.lon, B.lat
                    FROM isol_tower_3 A INNER JOIN towers B
                    ON A.out_site_id = B.site_id')
remove(isol_tower_1, isol_tower_2, isol_tower_3)

#ISOLATION MAP
isolation_map <- ggmap(senmap) +
  geom_point(aes(x=lon, y=lat, fill=callProp),
             size=5, colour="black", pch=21, data=isol_tower) +
  scale_fill_gradientn(colours=c("white","darkred"))
isolation_map
