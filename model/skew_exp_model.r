f <- rep(0,100)
for(i in seq(0,1,0.01)){ f[i*100+1] = 1/exp((-i+0.5)^2*12) }
plot(f,typ="l",lwd=3,col="red",xlab="Node Centrality Percentile (%)",ylab="Expected Skew of Neighbord Centralities")
for(i in seq(0,1,0.01)){ f[i*100+1]=  1/exp((-i+0.5)^2*8) }
lines(f,lwd=3,col="purple")
for(i in seq(0,1,0.01)){ f[i*100+1] =1/exp((-i+0.5)^2*6) }
lines(f,lwd=3,col="blue")
for(i in seq(0,1,0.01)){ f[i*100+1] =1/exp((-i+0.5)^2*4) }
lines(f,lwd=3,col="green")
for(i in seq(0,1,0.01)){ f[i*100+1] =1/exp((-i+0.5)^2*3) }
lines(f,lwd=3,col="brown")
for(i in seq(0,1,0.01)){ f[i*100+1]  =1/exp((-i+0.5)^2*2) }
lines(f,lwd=3,col="black")
legend(x = 40,y = 0.6,legend = c(expression(paste(gamma," = 12")),
                               expression(paste(gamma," = 8")),
                               expression(paste(gamma," = 6")),
                               expression(paste(gamma," = 4")),
                               expression(paste(gamma," = 3")),
                               expression(paste(gamma," = 2"))),lwd=rep(3,6),col=c("red","purple","blue","green","brown","black"))



for(i in 1:100){ f[i] = exp(-(2*i/4)) }
plot(f,typ="l",lwd=3,col="red",xlab="Standard Deviation",ylab="Geographical 'middle' score",xlim=c(2,25),ylim=c(0,1))
for(i in 1:100){ f[i] = exp(-(2*i/6)) }
lines(f,lwd=3,col="purple")
for(i in 1:100){ f[i] = exp(-(2*i/8)) }
lines(f,lwd=3,col="blue")
for(i in 1:100){ f[i] = exp(-(2*i/12)) }
lines(f,lwd=3,col="green")
for(i in 1:100){ f[i] = exp(-(2*i/20)) }
lines(f,lwd=3,col="brown")
for(i in 1:100){ f[i] = exp(-(2*i/40)) }
lines(f,lwd=3,col="black")
legend(x = 20,y = 1,legend = c(expression(paste(delta," = 4")),
                               expression(paste(delta," = 6")),
                               expression(paste(delta," = 8")),
                               expression(paste(delta," = 12")),
                               expression(paste(delta," = 20")),
                               expression(paste(delta," = 40"))),lwd=rep(3,6),col=c("red","purple","blue","green","brown","black"))

