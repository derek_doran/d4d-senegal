import urllib2
import codecs
from bs4 import BeautifulSoup

def get_urls(outfile,url,root):
    soup = BeautifulSoup(urllib2.urlopen(url),fromEncoding="utf-8")
    if len(soup.find_all('table')) < 1:
        for link in soup.find_all('a'):
            url = root + link.get('href')
            x = ["..","/."]
            if not any([z in url for z in x]):
                if "'" in url:
                   url=url.replace("'","39")
                print url
                get_urls(outfile,url,root)
    else:
        for row in soup.find_all('tr'):
            tds = row.find_all('td')
            if len(tds) > 0 and tds[1].text == 'city':
                outfile.write(unicode(tds[0].text+","+tds[4].text+","+tds[5].text+","+tds[7].text+"\n"))        
    
root = 'http://www.fallingrain.com'
html = urllib2.urlopen(root+"/world/SG").read()
soup = BeautifulSoup(html,fromEncoding="utf-8")
outfile = codecs.open("out.txt","w","utf-8-sig")
outfile.write(u"name,lat,lon,pop\n")
for link in soup.find_all('a'):
    url = root+link.get('href')
    if "/a/" in url:
        print url
        get_urls(outfile,url,root)
outfile.close()
