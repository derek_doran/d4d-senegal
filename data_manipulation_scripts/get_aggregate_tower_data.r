library(dplyr)
### This kind of merging needs to happen over every file, and then we need to aggregate everything together
### (at least, the edge attributes) into a single table
all_tower_data <- data.frame(out_site_id=character(),in_site_id=character(),num_calls=character(),total_duration=character(),stringsAsFactors=F)
for(file in list.files(path="~/data/senegal/SET1/voice/full_data_files/",full.names = T)){
  t1 <- Sys.time()
  cat(file,'\n')
  voice_calls_this_month <- read.csv(file,stringsAsFactors=F,header=F)
  names(voice_calls_this_month) <- c("timestamp","out_site_id","in_site_id","num_calls","total_duration")
  voice_calls_this_month <- group_by(voice_calls_this_month,out_site_id,in_site_id)
  voice_calls_this_month <- summarize(voice_calls_this_month,num_calls=sum(num_calls),total_duration=sum(total_duration))
  all_tower_data <- rbind(all_tower_data,voice_calls_this_month)
  all_tower_data <- group_by(all_tower_data,out_site_id,in_site_id)
  all_tower_data <- summarize(all_tower_data,num_calls=sum(num_calls),total_duration=sum(total_duration))
  difftime(Sys.time(),t1)
}
rm(t1)
rm(voice_calls_this_month)
rm(file)