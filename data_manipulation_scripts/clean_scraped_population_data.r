require(gmt)

cities <- read.csv("scraped_population_data.csv",header=T,encoding="UTF-8-BOM",stringsAsFactors=F)
colnames(cities) = c("name","lat","lon","pop")
cities <- cities[!duplicated(cities[,c("lat","lon")]),]
write.table(cities[,c("name","lat","lon")],file = "city_location_population.csv",sep = ",",quote=F,row.names=F)
plot(sort(cities$pop,decreasing = T),log="xy",typ="l",lwd=3,col="red",ylab="Population",xlab="rank")
abline(h=median(cities$pop),lwd=3,col="blue")
plot(sort(cities$pop[which(cities$pop > median(cities$pop))],decreasing = T),log="xy",typ="l",lwd=3,col="red",ylab="Population",xlab="rank")

if(exists("cities")){
  for(i in 1:length(cities$id)){
    closest_antenna_id = -1
    c_dist = 99999999
    for(j in 1:length(antenna$site_id)){
      d <- geodist(cities$lat[i],cities$lon[i],antenna$lat[j],antenna$lon[j],"km")
      if(d < c_dist){
        closest_antenna_id = antenna$site_id[j]
        c_dist <- d
      }
    }
    cities$closest_antenna[i] <- closest_antenna_id
    cities$antenna_dist[i] <- c_dist
    cat(i,'\n')
  } 
  rm(d)
  rm(i)
  rm(j)
  rm(c_dist)
  rm(closest_antenna_id)
}