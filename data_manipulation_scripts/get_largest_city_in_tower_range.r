require(gmt)

max_distance = 35 #35km is range for many GSM towers, see https://en.wikipedia.org/wiki/Cell_site
num_row = length(antenna$site_id)
biggest_cities_in_ant_range <- data.frame(ant_id = rep(-1,num_row), city_id = rep(-1,num_row), city_pop = rep(-1,num_row), city_dist = rep(-1,num_row))

for(i in 1:length(antenna$site_id)){
  biggest_city_id = -1
  city_pop = -1
  city_dist = 1
  for(j in 1:length(cities$id)){
    d <- geodist(cities$lat[j],cities$lon[j],antenna$lat[i],antenna$lon[i],"km")
    if(d <= max_distance){
      if(cities$pop[j] > city_pop){ 
        city_pop = cities$pop[j] 
        biggest_city_id = cities$id[j]
        city_dist = d
      }
    }
  }
  biggest_cities_in_ant_range$ant_id[i] = antenna$site_id[i]
  biggest_cities_in_ant_range$city_id[i] = biggest_city_id
  biggest_cities_in_ant_range$city_pop[i] = city_pop
  biggest_cities_in_ant_range$city_dist[i] = city_dist
  rm(biggest_city_id)
  rm(city_pop)
  rm(city_dist)
  rm(d)
  cat(i,'\n')
} 
rm(i)
rm(j)
rm(max_distance)
rm(num_row)
