require(fields)

antenna <- read.csv("SITE_ARR_LONLAT.CSV",header = T)
antenna$closest_city_id <- rep(-1,length(antenna[,1]))
for(k in 1:length(antenna$lat)){
  dist <- c()
  for(i in 1:length(cities[,1])){
    dist <- append(dist,rdist.earth(
      matrix(c(antenna$lon[k],antenna$lat[k]),ncol=2),
      matrix(c(cities$lon[i],cities$lat[i]),ncol=2), miles=F
    ))
  }
  cat(k,'\n')
  antenna$closest_city_id[k] <- cities$id[which(dist == min(dist))]
}

#chop this down into a network file and print it out. 
## Attributes? Number of calls, total call duration, number of sms