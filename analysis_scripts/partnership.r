require(sqldf)
gephi_1 <- sqldf('SELECT A.*, B.arr_id AS out_arr_id
                  FROM cell A INNER JOIN towers B ON
                  A.out_site_id = B.site_id')
gephi_2 <- sqldf('SELECT A.*, B.arr_id AS in_arr_id
                  FROM gephi_1 A INNER JOIN towers B ON
                  A.in_site_id = B.site_id')
gephi <- sqldf('SELECT out_arr_id AS Source, in_arr_id AS Target, SUM(num_calls) AS calls
                  FROM gephi_2 GROUP BY out_arr_id, in_arr_id')

#PARETO PARTNERS
pareto_1 <- sqldf('SELECT A.*, B.outDegree AS totalCalls
                  FROM gephi A INNER JOIN towers_grouped B
                  ON A.Source = B.arr_id')

pareto_1 <- pareto_1[order(pareto_1$Source, -pareto_1$calls),]

for(i in 1:nrow(pareto_1)){
  if(i==1){
    pareto_1[i,5] = pareto_1[i,3] / pareto_1[i,4]
  }
  else{
    if(pareto_1[i-1,1]==pareto_1[i,1]){
      pareto_1[i,5] = pareto_1[i-1,5] + (pareto_1[i,3] / pareto_1[i,4])
    }
    else{
      pareto_1[i,5] = pareto_1[i,3] / pareto_1[i,4]
    }
  }
}
colnames(pareto_1)[5] = "cumeProbInclSelf"

isolation$exclSelf <- isolation$totalCalls - isolation$selfCalls
pareto_2 <- sqldf('SELECT A.*, B.exclSelf AS totalCalls
                  FROM gephi A INNER JOIN isolation B
                  ON A.Source = B.arr_id
                  WHERE A.Source <> A.Target')
pareto_2 <- pareto_2[order(pareto_2$Source, -pareto_2$calls),]

for(i in 1:nrow(pareto_2)){
  if(i==1){
    pareto_2[i,5] = pareto_2[i,3] / pareto_2[i,4]
  }
  else{
    if(pareto_2[i-1,1]==pareto_2[i,1]){
      pareto_2[i,5] = pareto_2[i-1,5] + (pareto_2[i,3] / pareto_2[i,4])
    }
    else{
      pareto_2[i,5] = pareto_2[i,3] / pareto_2[i,4]
    }
  }
}
colnames(pareto_2)[5] = "cumeProbExclSelf"

pareto_1b <- sqldf('SELECT Source AS arr_id, COUNT(*) AS paretoIncl
                    FROM pareto_1 WHERE cumeProbInclSelf <= 0.8
                    GROUP BY arr_id')
pareto_2b <- sqldf('SELECT Source AS arr_id, COUNT(*) AS paretoExcl
                    FROM pareto_2 WHERE cumeProbExclSelf <= 0.8
                    GROUP BY arr_id')
pareto <- sqldf('SELECT A.arr_id, A.lat, A.lon, A.logCalls, A.selfProp,
                 B.paretoIncl, C.paretoExcl
                 FROM isolation A INNER JOIN pareto_1b B
                 ON A.arr_id = B.arr_id INNER JOIN pareto_2b C
                 ON B.arr_id = C.arr_id')
remove(pareto_1, pareto_1b, pareto_2, pareto_2b,gephi,gephi_1,gephi_2)
