require(igraph)
library(e1071)

g <- graph.edgelist(as.matrix(group_tower_activity[,c("out_arr_id","in_arr_id")]),directed = FALSE)
E(g)$weight <- as.numeric(group_tower_activity[,c("total_duration")])
g <- simplify(g)
#g <- delete.vertices(g, which(degree(g) < 1))
d = .87
V(g)$pagerank <- page.rank(graph = g,damping = d)$vector#/max(page.rank(graph = g,damping=d)$vector)
rm(d)

 g <- graph.edgelist(as.matrix(td[,c("out_site_id","in_site_id")]),directed = FALSE)
 E(g)$weight <- as.numeric(td[,c("total_duration")])
 g <- simplify(g)
 #g <- delete.vertices(g, which(degree(g) < 1))
 d = .87
 V(g)$pagerank <- page.rank(graph = g,damping = d)$vector/max(page.rank(graph = g,damping=d)$vector)
 rm(d)



