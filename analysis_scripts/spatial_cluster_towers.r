### Save this for validation -- to find the cities located 'central' within clusters of cell towers. 

geo.dist = function(df) {
  require(geosphere)
  d <- function(i,z){         # z[1:2] contain long, lat
    dist <- rep(0,nrow(z))
    dist[i:nrow(z)] <- distHaversine(z[i:nrow(z),1:2],z[i,1:2])
    return(dist)
  }
  dm <- do.call(cbind,lapply(1:nrow(df),d,df))
  return(as.dist(dm))
}
#d <- geo.dist(data.frame(long=antenna$lon, lat=antenna$lat, site_id=antenna$site_id))

get_dist = function(d,i,j){
  n = attr(d, "Size")
  return(d[n*(i-1) - i*(i-1)/2 + j-i])
} 

get_center = function(d){
  dist = 1500
  n = attr(d,"Size")
  count <- 50
  label <- rep(1,n)
  for(i in 1:n){
    dists <- c(0)
    for(k in 1:n){
      y <- get_dist(d,i,k)
      if(i != k &&  y < dist){
        dists <- append(dists,y)
      }
    }
    if(length(dists[dists > count])){
      label[i] <- 2
    }
  }
  return(label)
}
antenna$clust <- get_center(d)
source('~/git/R Projects/D4D Senegal/visualization_scripts/tower_activity_plot.r')