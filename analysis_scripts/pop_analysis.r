require(fields)

#distribution of populations across the country
plot(sort(cities$pop,decreasing = T),log="xy",typ="l",lwd=3,col="red",ylab="Population",xlab="rank")
abline(h=median(cities$pop),lwd=3,col="blue")
plot(sort(cities$pop[which(cities$pop > median(cities$pop))],decreasing = T),log="xy",typ="l",lwd=3,col="red",ylab="Population",xlab="rank")

#darou rahmane: distance vs. mean population
lon <- -17.36; lat <- 14.75
dist <- c()
for(i in 1:length(cities[,1])){
  dist <- append(dist,rdist.earth(
    matrix(c(lon,lat),ncol=2),
    matrix(c(cities$lon[i],cities$lat[i]),ncol=2), miles=F
  ))
}
mean_pop <- list()
mean_pop_per <- list()
for(i in 1:100) {mean_pop[[i]] <- c(0); mean_pop_per[[i]] <- c(0)}
for(i in 1:floor(max(dist))){
  #for(k in 1:100){
  k<-5;  mean_pop[[k]][i] <- mean(cities$pop[which(dist < k+i & dist > i-k)])
    if(i > 1){
      mean_pop_per[[k]][i] <- (mean_pop[[k]][i] - mean_pop[[k]][i-1])/mean_pop[[k]][i-1]
    }
  #}
}
plot(mean_pop[[5]][1:200],typ="l",lwd=3,col="red",xlab="Distance from Dakar (+/- 5km)",ylab="Mean Population",log="y")
plot(mean_pop_per[[5]][1:200],typ="l",lwd=3,col="blue",xlab="Distance from Dakar (+/- 5km)",ylab="Percent Change in Population")

#Louga: distance vs. mean population
lon <- -16.22; lat <- 15.62
dist <- c()
for(i in 1:length(cities[,1])){
  dist <- append(dist,rdist.earth(
    matrix(c(lon,lat),ncol=2),
    matrix(c(cities$lon[i],cities$lat[i]),ncol=2), miles=F
  ))
}
mean_pop <- list()
mean_pop_per <- list()
for(i in 1:100) {mean_pop[[i]] <- c(0); mean_pop_per[[i]] <- c(0)}
for(i in 1:floor(max(dist))){
  k <- 5
  mean_pop[[k]][i] <- mean(cities$pop[which(dist < k+i & dist > i-k)])
  if(i > 1){
    mean_pop_per[[k]][i] <- (mean_pop[[k]][i] - mean_pop[[k]][i-1])/mean_pop[[k]][i-1]
  }
}
plot(mean_pop[[5]][1:200],typ="l",lwd=3,col="red",xlab="Distance from Louga (+/- 5km)",ylab="Mean Population",log="y")
plot(mean_pop_per[[5]][1:200],typ="l",lwd=3,col="blue",xlab="Distance from Louga (+/- 5km)",ylab="Percent Change in Population")

#Theis: distance vs. mean population 14.783 / -16.967
lon <- -16.967; lat <- 14.783
dist <- c()
for(i in 1:length(cities[,1])){
  dist <- append(dist,rdist.earth(
    matrix(c(lon,lat),ncol=2),
    matrix(c(cities$lon[i],cities$lat[i]),ncol=2), miles=F
  ))
}
mean_pop <- list()
mean_pop_per <- list()
for(i in 1:100) {mean_pop[[i]] <- c(0); mean_pop_per[[i]] <- c(0)}
for(i in 1:floor(max(dist))){
  k <- 5
  mean_pop[[k]][i] <- mean(cities$pop[which(dist < k+i & dist > i-k)])
  if(i > 1){
    mean_pop_per[[k]][i] <- (mean_pop[[k]][i] - mean_pop[[k]][i-1])/mean_pop[[k]][i-1]
  }
}
plot(mean_pop[[5]][1:200],typ="l",lwd=3,col="red",xlab="Distance from Thies (+/- 5km)",ylab="Mean Population",log="y")
plot(mean_pop_per[[5]][1:200],typ="l",lwd=3,col="blue",xlab="Distance from Thies (+/- 5km)",ylab="Percent Change in Population")